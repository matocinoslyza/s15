console.log("Hello World")


// Prompt  the two numbers
let num1 = parseInt(prompt("Enter the first number: "));
let num2 = parseInt(prompt("Enter the second number: "));

//Get the total of the two numbers
let sum = num1 + num2;
let difference = num1 - num2;
let product = num1 * num2;
let quotient = num1 / num2;

// Apply conditions to the sum
if (sum < 10){
	console.log(sum);
	alert("The total is: " + parseInt(sum))
}else if (sum >= 10 && sum <=20){
	console.log(sum);
	alert("The difference is " + parseInt(difference))
}else if (sum >=21 && sum <=30){
	console.log(sum);
	alert("The product is " + parseInt(product))
}else if (sum >=31){
	console.log(sum);
	alert("The quotient is " + parseInt(quotient))
}

// 4 alert if the total is 10 or greater and console warning for total of 9 or less
if (sum <= 9) {
	console.warn("The total is 9 or less");
}else if(sum >= 10) {
	alert("The total is 10 or higher");
}	

// Prompt the user for their name and age
let name = prompt("Enter your name: ");
let age = parseInt(prompt("Enter your age: "));
// Condition for name and age
if (name == undefined || name == null || name.length <=0) {
	alert("Are you a time traveller?");
} else if(age == undefined || age == null || age.length <=0){
	alert("Are you a time traveller?");
} else {
	alert(`Your name is ${name}, your age is ${age}`);
}


// Create a function named isLegalAge which will check if the user's input from the previous prompt is of legal age:
function isOfLegalAge() {
	return 'You are of the legal age'
}

function isUnderAge() {
	return 'You are not allowed here'
}

let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
alert(`${legalAge}, ${name}`);


// Create a switch case statement that will check if the user's age input is within a certain set of expected input
switch(age){
	case 18:
		alert("You are now allowed to party.");
		break;
	case 21:
		alert("You are now part of the society.");
		break;
	case 65:
		alert("We thank you for your contribution to the society.");
		break;
	default:
		alert("Are you sure you're not an alien?");
}


// Create try catch finally statement to force an error
function isOfLegalAge(age){
	try{
		// Attempt to execute a code
		alerted(showAlert(age));
	}
	catch(error){
		console.log(typeof error);
		console.log(error.message)
	}
	finally{
		// Continue to execute code regardless of success or failure of code execution in the try block
		alert('This is another message regardless of success or failure of code');
	}
}

isOfLegalAge(56);
